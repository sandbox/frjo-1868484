<?php

/**
 * @file
 * Theme callbacks for the html5_media_formatters module.
 */

/**
 * Returns HTML for displaying an HTML5 <audio> tag.
 *
 * @param $variables
 *   An associative array containing:
 *   - file: Associative array of file data, which must include "uri".
 *   - controls: Boolean indicating whether or not controls should be displayed.
 *   - autoplay: Boolean indicating whether or not the audio should start playing automatically.
 *   - loop: Boolean indicating whether or not the audio should loop.
 *
 * @ingroup themeable
 */
function theme_html5_media_formatters_audio($variables) {
  $files = $variables['files'];
  $output = '';

  $audio_attributes = array();
  $download_links = array();
  if ($variables['controls']) {
    $audio_attributes['controls'] = 'controls';
  }
  if ($variables['autoplay']) {
    $audio_attributes['autoplay'] = 'autoplay';
  }
  if ($variables['loop']) {
    $audio_attributes['loop'] = 'loop';
  }
  if (!empty($variables['preload'])) {
    $audio_attributes['preload'] = $variables['preload'];
  }

  $output .= '<audio' . drupal_attributes($audio_attributes) . '>';
  $output .= t('Your browser does not support the audio element.');
  foreach ($files as $delta => $file) {
    $source_attributes = array(
      'src' => file_create_url($file['uri']),
      'type' => $file['filemime'],
    );
    $output .= '<source' . drupal_attributes($source_attributes) . '>';

    if ($variables['download_link']) {
      $download_links[] = array(
        'title' => !empty($file['description']) ? $file['description'] : $file['filename'],
        'href' => $source_attributes['src'],
      );
    }
  }
  $output .= '</audio>';

  if ($variables['download_link']) {
    $output .= theme('links', array('links' => $download_links, 'attributes' => array('class' => 'download-links download-links-audio')));
  }

  return $output;
}

/**
 * Returns HTML for displaying an HTML5 <video> tag.
 *
 * @param $variables
 *   An associative array containing:
 *   - file: Associative array of file data, which must include "uri".
 *   - controls: Boolean indicating whether or not controls should be displayed.
 *   - autoplay: Boolean indicating whether or not the video should start playing automatically.
 *   - loop: Boolean indicating whether or not the video should loop.
 *   - muted: Boolean indicating whether or not the sound should be muted.
 *   - width: Width, in pixels, of the video player.
 *   - height: Height, in pixels, of the video player.
 *
 * @ingroup themeable
 */
function theme_html5_media_formatters_video($variables) {
  $files = $variables['files'];
  $output = '';

  $video_attributes = array();
  $download_links = array();
  if ($variables['controls']) {
    $video_attributes['controls'] = 'controls';
  }
  if ($variables['autoplay']) {
    $video_attributes['autoplay'] = 'autoplay';
  }
  if ($variables['loop']) {
    $video_attributes['loop'] = 'loop';
  }
  if ($variables['muted']) {
    $video_attributes['muted'] = 'muted';
  }
  if (!empty($variables['preload'])) {
    $video_attributes['preload'] = $variables['preload'];
  }
  if (!empty($variables['width']) && !empty($variables['height'])) {
    $video_attributes['width'] = $variables['width'];
    $video_attributes['height'] = $variables['height'];
  }

  $output .= '<video' . drupal_attributes($video_attributes) . '>';
  $output .= t('Your browser does not support the video element.');
  foreach ($files as $delta => $file) {
    $source_attributes = array(
      'src' => file_create_url($file['uri']),
      'type' => $file['filemime'],
    );
    $output .= '<source' . drupal_attributes($source_attributes) . '>';

    if ($variables['download_link']) {
      $download_links[] = array(
        'title' => !empty($file['description']) ? $file['description'] : $file['filename'],
        'href' => $source_attributes['src'],
      );
    }
  }
  $output .= '</video>';

  if ($variables['download_link']) {
    $output .= theme('links', array('links' => $download_links, 'attributes' => array('class' => 'download-links download-links-video')));
  }

  return $output;
}
