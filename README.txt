Drupal HTML5 media formaters module:
------------------------------------
Maintainers:
  Fredrik Jonsson (http://drupal.org/user/5546)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
HTML5 media formaters provides formatters that will render a file
using the HTML5 audio and video tags.


Credit:
-------
Most of the code for this module comes from a patch posted to
http://drupal.org/node/1748952#comment-6411658 by Erik Erskine (ingaro).